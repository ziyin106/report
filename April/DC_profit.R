rm(list=ls())
library(readr)
library(ggplot2)
library(grid)
library(gridExtra)
library(Hmisc)
library(tidyverse)
library(lubridate)
library(plyr)
#### Clean Data ###
#### total ####
total_bal = read_csv("./balances/balances_Total_All.csv")
total_bal = total_bal %>% 
  filter(epoch>1522386473)  # select date >30 March

# change balance.csv time HMS - HM
time_string = data.frame(lapply(total_bal['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(total_bal['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
total_bal['utc'] = time_string['utc']

# merge bal, price, arbitrage
name = 'total'
total = cbind(total_bal,name)
total = mutate(total,
               time = ymd_hm(utc),
               price = (total - fiat)/eth
)

# Select variables in correct time 
total_s <- total %>% 
  filter(epoch>1521033902)  %>% # 14 Mar: new money enter 
  select(-utc)
colnames(total_s) = c("epoch",'bal_eth','bal_fiat','bal_total',
                      'exchange','time','market_price')

#### gdax ####
gdax_bal = read_csv("./balances/balances_gdax.csv")
gdax_bal = gdax_bal %>% 
  filter(epoch>1522386473)  # select date >30 March

gdax_eth_prices = read_csv("./prices/prices_gdax_eur.csv")
gdax_arb <- read_csv("./arb_fetcher/ma_eth_bitcoinindo.csv")
time_string2 = character()

# change arb.csv time HMS - HM
time_string = data.frame(lapply(gdax_arb['time'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(gdax_arb['time'])){
  time_string[i,1] = substr(time_string[i,1], start = 5, stop = 21)
  time_string2[i]= paste(substr(time_string[i,1], start = 8, stop = 11), "-" , '0', match(substr(time_string[i,1], start = 1, stop = 3),month.abb), '-', substr(time_string[i,1], start = 5, stop = 6), substr(time_string[i,1], start = 12, stop = 17), sep ='')
}
gdax_arb['time'] = time_string2
colnames(gdax_arb) = c("utc",'day','month','week','hour','fhour','now')

# change prices.csv time HMS - HM
colnames(gdax_eth_prices)[1]=c('utc')

time_string = data.frame(lapply(gdax_eth_prices['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(gdax_eth_prices['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
gdax_eth_prices['utc'] = time_string['utc']



# change balance.csv time HMS - HM
time_string = data.frame(lapply(gdax_bal['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(gdax_bal['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
gdax_bal['utc'] = time_string['utc']

# merge bal, price, arbitrage
gdax <- merge(gdax_bal, gdax_eth_prices,by = c("utc"),all.x = TRUE)
gdax <- merge(gdax, gdax_arb,by = c("utc"))
name = 'gdax'
gdax = cbind(gdax,name)
gdax = mutate(gdax,
              time = ymd_hm(utc)
)

# Select variables in correct time 
gdax_s <- gdax %>% 
  filter(utc>1522386473)  %>% # 14 Mar: new money enter 
  select(-utc,-epoch.y)
colnames(gdax_s) = c("epoch",'bal_eth','bal_fiat','bal_total','market_price','market_ethvol',
                     'arb_day','arb_month','arb_week','arb_hour','arb_fhour','arb_now',
                     'exchange','time')

#### bitcoincoid ####
bitcoincoid_bal = read_csv("./balances/balances_bitcoincoid.csv")
bitcoincoid_bal = bitcoincoid_bal %>% 
  filter(epoch>1522386473)  # select date >30 March

bitcoincoid_eth_prices = read_csv("./prices/prices_bitcoincoid_eth.csv")
bitcoincoid_arb <- read_csv("./arb_fetcher/ma_eth_bitcoinindo.csv")

time_string2 = character()
# change arb.csv time HMS - HM
time_string = data.frame(lapply(bitcoincoid_arb['time'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(bitcoincoid_arb['time'])){
  time_string[i,1] = substr(time_string[i,1], start = 5, stop = 21)
  time_string2[i]= paste(substr(time_string[i,1], start = 8, stop = 11), "-" , '0', match(substr(time_string[i,1], start = 1, stop = 3),month.abb), '-', substr(time_string[i,1], start = 5, stop = 6), substr(time_string[i,1], start = 12, stop = 17), sep ='')
}
bitcoincoid_arb['time'] = time_string2
colnames(bitcoincoid_arb) = c("utc",'day','month','week','hour','fhour','now')

# change prices.csv time HMS - HM
time_string = data.frame(lapply(bitcoincoid_eth_prices['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(bitcoincoid_eth_prices['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
bitcoincoid_eth_prices['utc'] = time_string['utc']

# change balance.csv time HMS - HM
time_string = data.frame(lapply(bitcoincoid_bal['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(bitcoincoid_bal['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
bitcoincoid_bal['utc'] = time_string['utc']

# merge bal, price, arbitrage
bitcoincoid <- merge(bitcoincoid_bal, bitcoincoid_eth_prices,by = c("utc"))
bitcoincoid <- merge(bitcoincoid, bitcoincoid_arb,by = c("utc"))
name = 'bitcoincoid'
bitcoincoid = cbind(bitcoincoid,name)
bitcoincoid = mutate(bitcoincoid,
            time = ymd_hm(utc)
  )

# Select variables in correct time 
bitcoincoid_s <- bitcoincoid %>% 
  filter(epoch>1521033902)  %>% # 14 Mar: new money enter 
  select(-utc,-Indoutc_time)
colnames(bitcoincoid_s) = c("epoch",'bal_eth','bal_fiat','bal_total','market_bid','market_ask','market_ethvol',
                            'market_price','arb_day','arb_month','arb_week','arb_hour','arb_fhour','arb_now',
                            'exchange','time')

#### quadrigacx ####
quadrigacx_bal = read_csv("./balances/balances_quadrigacx.csv")
quadrigacx_eth_prices = read_csv("./prices/prices_quadrigacx_eth.csv")
quadrigacx_arb <- read_csv("./arb_fetcher/ma_eth_bitcoinindo.csv")
time_string2 = character()
# change arb.csv time HMS - HM
time_string = data.frame(lapply(quadrigacx_arb['time'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(quadrigacx_arb['time'])){
  time_string[i,1] = substr(time_string[i,1], start = 5, stop = 21)
  time_string2[i]= paste(substr(time_string[i,1], start = 8, stop = 11), "-" , '0', match(substr(time_string[i,1], start = 1, stop = 3),month.abb), '-', substr(time_string[i,1], start = 5, stop = 6), substr(time_string[i,1], start = 12, stop = 17), sep ='')
}
quadrigacx_arb['time'] = time_string2
colnames(quadrigacx_arb) = c("utc",'day','month','week','hour','fhour','now')

# change prices.csv time HMS - HM
time_string = data.frame(lapply(quadrigacx_eth_prices['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(quadrigacx_eth_prices['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
quadrigacx_eth_prices['utc'] = time_string['utc']

# change balance.csv time HMS - HM
time_string = data.frame(lapply(quadrigacx_bal['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(quadrigacx_bal['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
quadrigacx_bal['utc'] = time_string['utc']

# merge bal, price, arbitrage
quadrigacx <- merge(quadrigacx_bal, quadrigacx_eth_prices,by = c("utc"))
quadrigacx <- merge(quadrigacx, quadrigacx_arb,by = c("utc"))
name = 'quadrigacx'
quadrigacx = cbind(quadrigacx,name)
quadrigacx = mutate(quadrigacx,
                    time = ymd_hm(utc)
)

# Select variables in correct time 
quadrigacx_s <- quadrigacx %>% 
  filter(epoch>1521033902)  %>% # 14 Mar: new money enter 
  select(-utc)

colnames(quadrigacx_s) = c("epoch",'bal_eth','bal_fiat','bal_total','market_bid','market_ask','market_ethvol',
                           'market_price','arb_day','arb_month','arb_week','arb_hour','arb_fhour','arb_now',
                           'exchange','time')

  

#### bitbay ####
bitbay_bal = read_csv("./balances/balances_bitbay.csv")
bitbay_eth_prices = read_csv("./prices/prices_bitbay_eth.csv")
bitbay_arb <- read_csv("./arb_fetcher/ma_eth_bitcoinindo.csv")
time_string2 = character()
# change arb.csv time HMS - HM
time_string = data.frame(lapply(bitbay_arb['time'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(bitbay_arb['time'])){
  time_string[i,1] = substr(time_string[i,1], start = 5, stop = 21)
  time_string2[i]= paste(substr(time_string[i,1], start = 8, stop = 11), "-" , '0', match(substr(time_string[i,1], start = 1, stop = 3),month.abb), '-', substr(time_string[i,1], start = 5, stop = 6), substr(time_string[i,1], start = 12, stop = 17), sep ='')
}
bitbay_arb['time'] = time_string2
colnames(bitbay_arb) = c("utc",'day','month','week','hour','fhour','now')

# change prices.csv time HMS - HM
time_string = data.frame(lapply(bitbay_eth_prices['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(bitbay_eth_prices['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
bitbay_eth_prices['utc'] = time_string['utc']

# change balance.csv time HMS - HM
time_string = data.frame(lapply(bitbay_bal['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(bitbay_bal['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
bitbay_bal['utc'] = time_string['utc']

# merge bal, price, arbitrage
bitbay <- merge(bitbay_bal, bitbay_eth_prices,by = c("utc"))
bitbay <- merge(bitbay, bitbay_arb,by = c("utc"))
name = 'bitbay'
bitbay = cbind(bitbay,name)
bitbay = mutate(bitbay,
                time = ymd_hm(utc)
)

# Select variables in correct time 
bitbay_s <- bitbay %>% 
  filter(epoch>1521033902)  %>% # 14 Mar: new money enter 
  select(-utc)

colnames(bitbay_s) = c("epoch",'bal_eth','bal_fiat','bal_total','market_bid','market_ask','market_ethvol',
                       'market_price','arb_day','arb_month','arb_week','arb_hour','arb_fhour','arb_now',
                       'exchange','time')

#### livecoin ####
livecoin_bal = read_csv("./balances/balances_livecoin.csv")
livecoin_eth_prices = read_csv("./prices/prices_livecoin_eth.csv")
livecoin_arb <- read_csv("./arb_fetcher/ma_eth_bitcoinindo.csv")
time_string2 = character()
# change arb.csv time HMS - HM
time_string = data.frame(lapply(livecoin_arb['time'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(livecoin_arb['time'])){
  time_string[i,1] = substr(time_string[i,1], start = 5, stop = 21)
  time_string2[i]= paste(substr(time_string[i,1], start = 8, stop = 11), "-" , '0', match(substr(time_string[i,1], start = 1, stop = 3),month.abb), '-', substr(time_string[i,1], start = 5, stop = 6), substr(time_string[i,1], start = 12, stop = 17), sep ='')
}
livecoin_arb['time'] = time_string2
colnames(livecoin_arb) = c("utc",'day','month','week','hour','fhour','now')

# change prices.csv time HMS - HM
time_string = data.frame(lapply(livecoin_eth_prices['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(livecoin_eth_prices['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
livecoin_eth_prices['utc'] = time_string['utc']

# change balance.csv time HMS - HM
time_string = data.frame(lapply(livecoin_bal['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(livecoin_bal['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
livecoin_bal['utc'] = time_string['utc']

# merge bal, price, arbitrage
livecoin <- merge(livecoin_bal, livecoin_eth_prices,by = c("utc"))
livecoin <- merge(livecoin, livecoin_arb,by = c("utc"))
name = 'livecoin'
livecoin = cbind(livecoin,name)
livecoin = mutate(livecoin,
                  time = ymd_hm(utc)
)

# Select variables in correct time 
livecoin_s <- livecoin %>% 
  filter(epoch>1521033902)  %>% # 14 Mar: new money enter 
  select(-utc)

colnames(livecoin_s) = c("epoch",'bal_eth','bal_fiat','bal_total','market_bid','market_ask','market_ethvol',
                         'market_price','arb_day','arb_month','arb_week','arb_hour','arb_fhour','arb_now',
                         'exchange','time')

#### wex ####
wex_bal = read_csv("./balances/balances_wex.csv")
wex_eth_prices = read_csv("./prices/prices_wex_eth.csv")
wex_arb <- read_csv("./arb_fetcher/ma_eth_bitcoinindo.csv")
time_string2 = character()
# change arb.csv time HMS - HM
time_string = data.frame(lapply(wex_arb['time'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(wex_arb['time'])){
  time_string[i,1] = substr(time_string[i,1], start = 5, stop = 21)
  time_string2[i]= paste(substr(time_string[i,1], start = 8, stop = 11), "-" , '0', match(substr(time_string[i,1], start = 1, stop = 3),month.abb), '-', substr(time_string[i,1], start = 5, stop = 6), substr(time_string[i,1], start = 12, stop = 17), sep ='')
}
wex_arb['time'] = time_string2
colnames(wex_arb) = c("utc",'day','month','week','hour','fhour','now')

# change prices.csv time HMS - HM
time_string = data.frame(lapply(wex_eth_prices['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(wex_eth_prices['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
wex_eth_prices['utc'] = time_string['utc']

# change balance.csv time HMS - HM
time_string = data.frame(lapply(wex_bal['utc'], as.character), stringsAsFactors=FALSE)
for (i in 1:nrow(wex_bal['utc'])){
  time_string[i,1] = substr(time_string[i,1], start = 1, stop = 16)
}
wex_bal['utc'] = time_string['utc']

# merge bal, price, arbitrage
wex <- merge(wex_bal, wex_eth_prices,by = c("utc"))
wex <- merge(wex, wex_arb,by = c("utc"))
name = 'wex'
wex = cbind(wex,name)
wex = mutate(wex,
             time = ymd_hm(utc)
)

# Select variables in correct time 
wex_s <- wex %>% 
  filter(epoch>1521033902)  %>% # 14 Mar: new money enter 
  select(-utc)

colnames(wex_s) = c("epoch",'bal_eth','bal_fiat','bal_total','market_bid','market_ask','market_ethvol',
                    'market_price','arb_day','arb_month','arb_week','arb_hour','arb_fhour','arb_now',
                    'exchange','time')

#### stack and save the data ####
dt = rbind.fill(total_s,gdax_s,bitcoincoid_s,quadrigacx_s,bitbay_s,livecoin_s,wex_s)
dt = dt %>% 
  filter(epoch>1522386473)  # select date >30 March

summary(dt)
saveRDS(dt, file = "dt.rds")
