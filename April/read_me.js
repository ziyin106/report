// log on to balance server
ssh root@165.227.82.162

// log on to prices server
ssh root@67.205.179.0

// only bitcoincoid & livecoin can be automatically download transcation, others need manually download. 

// 1. change the 'April' to 'Month' in execute.sh
// 2. in terminal first give the permission for the shell file
sudo chmod 755 /Users/Zi/Dropbox/alfie/April/execute.sh 

// 3. run it 
/Users/Zi/Dropbox/alfie/April/execute.sh 


// Manually execute
// Open local terminal : Download all the balance data
rsync -avz -e ssh root@165.227.82.162:/root/alfie-eth/balances /Users/Zi/Dropbox/alfie/April --exclude='env' --exclude='*.py'

// all the prices data
rsync -avz -e ssh root@67.205.179.0:/root/alfie-eth/prices /Users/Zi/Dropbox/alfie/April --exclude='*.py'

// arb data
rsync -avz -e ssh root@67.205.179.0:/root/alfie-eth/arb_fetcher /Users/Zi/Dropbox/alfie/April --exclude='*.js'