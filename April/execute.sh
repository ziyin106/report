#!/bin/bash 
node /Users/Zi/alfie-eth/bitcoincoid/hist.js > /Users/Zi/bitcoincoid_trans.csv
mv /Users/Zi/bitcoincoid_trans.csv /Users/Zi/Dropbox/alfie/april/trans/

node /Users/Zi/alfie-eth/livecoin/hist.js > /Users/Zi/livecoin_trans.csv
mv /Users/Zi/livecoin_trans.csv /Users/Zi/Dropbox/alfie/april/trans/

node /Users/Zi/alfie-eth/gdax/hist.js > /Users/Zi/gdax_trans.csv
mv /Users/Zi/gdax_trans.csv /Users/Zi/Dropbox/alfie/april/trans/



rsync -avz -e ssh root@165.227.82.162:/root/alfie-eth/balances /Users/Zi/Dropbox/alfie/april --exclude='env' --exclude='*.py'

rsync -avz -e ssh root@159.89.130.68:/root/alfie-eth/prices /Users/Zi/Dropbox/alfie/april/prices --exclude='*.py'

rsync -avz -e ssh root@159.89.130.68:/root/alfie-eth/arb_fetcher /Users/Zi/Dropbox/alfie/april/arb_fetcher --exclude='*.js'

cd /Users/Zi/alfie-eth/prices
python /Users/Zi/alfie-eth/prices/Pricefetcher_gdax_eth_usd.py 
mv /Users/Zi/alfie-eth/prices/prices_gdax_usd.csv /Users/Zi/Dropbox/alfie/april/prices/

cd /Users/Zi/alfie-eth/prices
python /Users/Zi/alfie-eth/prices/Pricefetcher_gdax_eth.py 
mv /Users/Zi/alfie-eth/prices/prices_gdax_eur.csv /Users/Zi/Dropbox/alfie/april/prices/
