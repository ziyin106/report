
INSTALLING PYTHON 2.7 ON A NEW SERVER (if not already installed).

1. Check to see if installed:

python -- version

If something like this appears: "The program 'python' can be found in the following packages: * python-minimal * python3"... then python2.7 is not installed and you'll need to install it.

2. Note: before continuing, you will probably want to do a quick sudo apt-get update, sudo apt-get upgrade, and sudo apt-get dist-upgrade (please do note exactly what these commands are in fact doing; I'm assuming a fresh install here.)

3. Installing python 2.7 is as easy as:

sudo apt-get install python2.7

4. sudo apt-get install python-pip

5. sudo apt-get install python3-pip

6. pip install virtualenv

7. python --version should now show python2.7.X


CREATING AND ACTIVATING A VIRTUAL ENVIRONMENT

1. virtualenv env

2. source env/bin/activate


INSTALLING REQUIREMENTS.TXT

1. With the virtualenv activated, run:

pip install -r requirements.txt

