---
title: "March Report"
author: "Zi Yin"
date: "31/03/2018"
output:
  html_document:
    toc: true # table of content true
    toc_depth: 3  # upto three depths of headings (specified by #, ## and ###)
    number_sections: true  ## if you want number sections at each table header
    theme: united  # many options for theme, this one is my favorite.
    highlight: tango  # specifies the syntax highlighting style
  html_notebook: 
    toc: yes
runtime: shiny
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

```

```{r load dt balance data, include=FALSE}
rm(list=ls())
library(readr)
library(ggplot2)
library(grid)
library(gridExtra)
library(Hmisc)
library(tidyverse)
library(lubridate)
library(data.table)
library(ecm)
library(urca)
library(vars)
library(plyr) 
bitbucketURL <- ("https://bitbucket.org/ziyin106/report/raw/7c7fa46f6c558a28a9da90dfa0a14f56c7ea24fe/March2/dt.rds")
download.file(bitbucketURL,"dt.rds", method="curl")
dt <- readRDS("dt.rds")

calprofit = function(x){
  log(x/lag(x,1))
}

dt = data.table(dt)
profit= dt[,calprofit(bal_total),by=exchange]
profit = profit[,2]
dt = cbind(dt,profit)
colnames(dt)[17] <- "profit"
cum_profit = dt[,cumsum(ifelse(is.na(profit), 0, profit)),by = exchange]
cum_profit = cum_profit[,2]
dt = cbind(dt,cum_profit)
colnames(dt)[18] <- "cum_profit"
dt = mutate(dt,
       dif_nd = arb_now - arb_day,
       dif_nh = arb_now - arb_hour,
       dif_dm = arb_day - arb_month,
       dif_nw = arb_day - arb_week,
       bas = market_ask/market_bid)

total = dt %>% 
  filter(exchange == 'total') %>% 
  mutate(tb_filter = bal_total - 40*market_price,
         profit_filter = calprofit(tb_filter),
         cum_profit_filter =cumsum(ifelse(is.na(profit_filter), 0, profit_filter)) )


```
```{r load dt_trans and gb data, include=FALSE}
bitbucketURL <- ("https://bitbucket.org/ziyin106/report/raw/7c7fa46f6c558a28a9da90dfa0a14f56c7ea24fe/March2/dt_trans.rds")
download.file(bitbucketURL,"dt.rds", method="curl")
dt_trans <- readRDS("dt.rds")
bitbucketURL <- ("https://bitbucket.org/ziyin106/report/raw/7c7fa46f6c558a28a9da90dfa0a14f56c7ea24fe/March2/dt_gv.rds")
download.file(bitbucketURL,"dt.rds", method="curl")
global_vol = readRDS("dt.rds")
# select transaction data (livecoin record the deposite and withdraw)
dt_trans = dt_trans %>% 
  filter(price>0)

```

# Introduction

This report aims to improve the performances of various traders in different exchanges based on the data fetched from traders and exchanges.

## Strategy Summary  

The trader is continuously bidding and asking based on two different strategies: MA and BAS. 

The BAS strategy (also called scalping) determins the 'market maker (limit order)' nature of the trader, where the advanatages are 1) transaction fee is lower 2) profit gaining on the spread and 3) relative better price compared to market takers. The disadvantages are 1) take more time to be filled 2) less profitbale when there are more 'smarter traders' in the exchange. For example, when the smart trader correctly predict the upward trend, they will fill our ask (as a taker) hence we lose the potential gain. **The BAS strategy works best in a price stable (low volatile), 'average smart', high BAS and high volume market.** And most important, the trader should be super fast to cancel the previous order and update a new one based on the newest order book to make sure we are competitive but not cheap. The common scalping implementation can be simply bid/ask the price of the second (3rd or whatever) most competitive in the order book, which is called balanced scalping in a sence that bid and ask are treat equally. However, in cryptocurrency market, as it is very volatile, we may want to use unbalanced scalping. For example, if we predict next 5 mins the price will decline, we may have more competitive ask price (say 2nd of orderbook) but less competitive bid price (10th of orderbook). 

The way we predict the future price is using MA strategy. If MA_now - MA_day > 0, we will have more competitive ask (as we expect the price will decline to the long trend, which is MA_day) but less competitive bid. 

The combination of these two strategies could 1) make scalping profit when the difference of MA of short and long run is small, which means we are in stable market 2) Avoid potential huge risk and are able to ride the upward trend when the difference is salient, which means in volatile market. All in all, we are using MA to determine if the upcoming market is volatile or stable by looking at the difference between MA_now and MA_day, if it's stable, BAS will work, if it's unstable, MA will work. There are only two things need to be determined afterwards: price and volume. 

## Model specification

Let's first speficy the base model in order to determine the price and size of the trader's bid/ask in every second (take bid as example): 

$$bid_{size} = bal_{fiat} * 0.2$$ 

$$bid_{price} = (MA_{day}/MA_{now})*bid_{10th}*g(x)$$

Notice here we use the 10th bidder in the order book as a reference point, and every trades will take the 20% of the fiat balance. For example, if MA_now < MA_day, we should buy crypto, so we surpass the 10th bidder and bid higher with the intention to buy crypto. 

Second, we change the MA in the local exchange price in the above base model to a ratio of price in leader market and local market, where we make the assumption there is a lead-lag relationship between small and big exchanges. At the moment, the function g(x) is just a constant = 1, however from (3) - (6), we will expand our base model by adding more variables into g(x).

Third, in order to keep high trading volume we have to ensure both fiat and crypto have enough balance, so we add target local balance (0.5) required profit level, this parameter is in a range of (0 - 0.005), a function of local balance. e.g., if we have very low fiat bal, even though we should still bid according to scalping trategy,  we don't want to be too competitive in the bid market, so the required profit is higher. 

Fourth, for the same reasoning we add the target global balance (0.45) required profit level (0 - 0.02). It is four time bigger than local balance, which may influence a lot for the determination of price. 

Fifth, we add the exchange transcation fee and expected profit margin (0.0005) for each trade.

Lastly, we add the additional profit level which is a function of MA_day - MA_month to cope with the long run trend. 

###Parameters in summary:

* Fee: the lower the better

* Additional profit: mean weight = 0.004, the higher the higher mark-up each trade, less volume, we should put more/less weight depend on the correctless of lead-lag relationship 

* Profit margin: the higher the higher mark-up each trade, but less volume

* Taget lb: 0.5

* Taget gb (crypto): 0.45, the higher the risky, but also lose the opportunity to win

* Required lb profit: 0.005, the higher the higher mark-up each trade, less volume, more weight on BAS strategy

* Required gb profit: 0.02, the higher the higher mark-up each trade, less volume, make overall trader less risky

* StepSize = minumum allowed unit in exchanges

* Approximate price = 1150, the higher the less crypto balance ratio we will have hence the safer

* TradeSize: 0.2, the higher means we put more weight (belief) on MA, as it will make crypto balance ratio change a lot. It also means more risk. For example, compared 0.01 size bid and 0.1 size bid with price = 500, the trader might want to update the price to 501 but it has already been taken by others. 

###Variables in summary:

* MA_now - MA_day: -0.02 to 0.02, detail see 5.1

* MA_day - MA_month: 0.001 to 0.006, detail see 5.1

### Conclusion
If the lead-lag relationship is salient (based on 6.3 Granger test; predict power 4.4; actual cumulative profit 4.1.3), we should put more weight on MA by 

1. Augmenting MA_now - MA_day, times a multiplier say 1.2
2. Augmenting MA_day - MA_month
3. Decreasing the weights on lb, gb required profit (especially 0.02 for gb), profit margin 
4. Increasing the trade size (say 0.3)

If the lead-lag relationship is not obvious, but the fee of the exchange is low, volume is high and the BAS is high, we should put more weight on BAS by

1. Mitigate the effect of  MA_now - MA_day, times a multiplier say 0.8
2. Mitigate the effect of  MA_day - MA_month
3. Decreasing the weights on lb, gb, profit margin 
4. Decreasing the trade size (say 0.1)

# Data 
* Balance fetcher (FB): 20 mins; fiat and eth balance for every traders

* Arb fetcher (FB): every minute; MA (now, hour, 4hour, week, month)

* Exchange price fetcher (Public API): every minute; bid, ask, last second transcation price and market depth for eth/fiat and btc/fiat.

* Transaction fetcher (Private API): every time any trader make any transction; transaction volume, price and fee


Notice here I merge balance, fetcher and exchange price together and call it balance data, which is recorded every 20 minutes.

## Data Summary balance data
Let's first take a closer look at the brief summary of balance data.
```{r summary of bal, echo = FALSE}
summary(dt)
```
* Observations: 3-14 to 3-31 (1200 data for each Ex); 9 exchanges (I also included 'total balance' as an exchange so together we have 8 + 1 = 9); n = 1200 * 9 = 10800

* Profit is calculated using the log difference of the balance at t and t-1

* Cum_profit is the cumulative sum of profit, which will be the measure of performance in the following analysis.

Notice here NAs in market variables is due to missing data in bitcoincoid (have been correctly recorded now); NAs in arb data is because 'total balance' do not have arb variables. 

## Data Summary transaction data
```{r summary of transaction, echo = FALSE}
summary(dt_trans)
```
* Observations: 3-01 to 3-26; 6 exchanges (drop huobi); n = 8367

* Tha last variable (exchange) provides the number of transcation for each Ex

Notice here NAs in fee is due to missing data in bitbay, it doesn't provide the fee data and I have not figure out how to fetch the transcation using python.

# Performance
The main way we evaluate performance of traders is using the cumulative profit over time. However, we also want to know whether our traders hit the balance targets we pre-assigned and how much volume they achieve. We are also interested in the theoretical performance (prediction power) of MA strategy. 

## Profit
The cumulative profit provides us the overall picture of trader's performance. 

### Overall performance
Here the first graph shows the raw cumulative profit of all traders across time. However, it may not necessary reflect the performance of traders as the eth price droped 40% (from 670 to 400). The second graph shows the adjusted cumulative profit which has deducted 40 eth from the total balance (bal_total - 40*market_price for every 20 mins). Nevertheless it only alleviates the overall price effect instead of removing it. 

```{r pressure, echo=FALSE}
tp = ggplot(data = total) +
  geom_line(aes(x=time,y=cum_profit)) 

tp_filter = ggplot(data = total) +
  geom_line(aes(x=time,y=cum_profit_filter)) 

tp_price = ggplot(data = total) +
  geom_line(aes(x=time,y=market_price)) 

#pdf('total_profit.pdf',paper='special')
grid.newpage()
grid.draw(rbind(ggplotGrob(tp), ggplotGrob(tp_filter), ggplotGrob(tp_price),size = "last"))

```


From the above three graph we could conclude:

* Trader's profit is influence a lot by the price movement (BAS profit can be easily wiped out, MA profit is hard to maintain); hedge should mitigate this problem

* After getting ride of the 40 eth movement, the trader shows a positive trend 

* In order to implement MA strategy completely, we should  

      + Make sure the predict power is high (without any fraction, see 3.4)
      
      + Allieviate the fraction of implementation (decrease lb, gb required profit leve)
      
      + Exchange has lower fee and high volume 
      
      + Our competitor is 'average smart', one of the drawback of this report is lack of competitor analysis -> I will spend some time look at the order book change in different exchange to see how other algo trader put orders. For example, if someone else use the same or more sophisticated auto trader than us, it is hard to make competitive order even though our prediction is correct (as our order can't be filled).

### Interactive graph with varying borrowing eth
```{r, echo=FALSE}
ggplot(data = total) +
  geom_line(aes(x=time,y=bal_eth)) 
```

The first graph give the actual eth balance of all traders over time. 
```{r, echo = FALSE}

sliderInput("eth", "Amount of borrowed eth:", min = 0, max = 70, value = 0)


renderPlot({
total = dt2 %>% 
  filter(exchange == 'total') %>% 
  mutate(tb_filter = bal_total - input$eth*market_price,
         profit_filter = calprofit(tb_filter),
         cum_profit_filter =cumsum(ifelse(is.na(profit_filter), 0, profit_filter)) )

ggplot(data = total) +
  geom_line(aes(x=time,y=cum_profit_filter)) 



})
```

As we can see from the above interactive graph, if we can borrow more eth, the performance will be better. 

### Individual performance
This part trying to answer the questions like which trader is the main driver for the overall performance and which one perform badly.
```{r, echo = FALSE}
neworder <- c('total','bitcoincoid','gdax','quadrigacx','livecoin','independentreserve','bitbay','kraken','huobipro')
dt2 <- arrange(transform(dt,
                           exchange=factor(exchange,levels=neworder)),exchange)

ggplot(data = dt2) +
  geom_line(aes(x=time,y=cum_profit)) +
  facet_wrap(~exchange,nrow=3,scale ='free')

```

Notice there there are lot of inter-exchange money movement which lead to discontinuity in the graphs that distract our attention. It should be better presented in April. However, we could still conclude that:

* Performance order: bitcoincoid > gdax > quadrigacx > livecoin > others

* Bitcoincoid: 26.2eth Before transfering money into performs well, after the performance seems decrease. 

* gdax: Stable, robust to increase or decrease of the market

* quadrigacx: 16eth Perform well in stable (and relative stable) market 

* livecoin: 11eth Perform well in increasing and stable market

* bitbay: 7.2eth badly perform (-21% till Mar 19)

* independentreserve: 3eth badly perform (-22% till Mar 19)

* kraken: 2.54eth badly perform (-25% till Mar 19)

* huobipro: 1eth badly perform (-25% till Mar 19)

Based on the above graph, I would like to keep bitcoincoid > gdax > quadrigacx > livecoin these four traders.

## Balance

* The target global balance (gb) is 0.55, after adjusting the eth =1150 usd, the gb = 0.7

* The target local balance (lb) is 0.5

* The more the balance is deviate from the target balance, the more it should generate profit from predicted MA (the reason for the gap b/w target and actual lb is that MA difference is greater)

* The less the balance is deviate from the target balance (it is still changeing), the more it should generate profit from BAS

```{r, echo = FALSE}
ggplot(data = dt2) +
  geom_line(aes(x=time,y=bal_fiat/bal_total)) +
  facet_wrap(~exchange,nrow=3)

ggplot(data = dt2) +
  geom_boxplot(aes(x=time,y=bal_fiat/bal_total,group = 1)) +
  facet_wrap(~exchange,nrow=3)

```

The first graph shows the balance change across time for each exchange, the second shows the boxplot which give the mean(the middle bold line), 0.25 and 0.75 quatlie and maximum and minimum of each exchange balance.

* gb is quite health, it means most of the time bid ask spread(BAS) is the main motivation of the trade, however, MA also plays important role when necessary. This is desired as we expect that BAS could make profit when the market is stable and MA save as from market crash and help us ride the market rocket. In other sense, we could potentially put too much weight on gb (0.02) compared to local balance.

* Desired lb: Even though target lb = 0.5, we expect a high than 0.5 fiat ratio on average over time as the market is decreasing overall (A correct MA strategy will lead to a higher lb in the given periods); the wider the box, the more MA strategy determine the trade direction, however we do not know if the MA prediction is good or not

* Bitcoincoid: MA plays a main part, if MA predictction is precise (which is proved in 3.4) then it can explains why it makes money even when market price crash 

* gdax: Mean is much the same as bitcoincoid, however it is much stable to lock fiat ratio in a narrow range, which explain why the performance is robust in different market condition (increase, decrease, stable)

* quadrigacx: Very similar to bitcoincoid but unfortunately mean is lower, around 0.6, which explains why it is not as profitable as bitcoincoid 

* livecoin: good, mean is 0.8; could be better if more transactions occur

* bitbay: fair, mean is 0.65; could be better if more transactions occur

* independentreserve: worst, mean is 0.3; not much trades

* kraken: bad, mean is below 0.5; frequency is too much

* huobipro: fair; frequency is too much

Based on the balance graphs, I would like to keep livecoin > bitcoincoid > gdax > bitbay > quadrigacx  these five traders.

Comment: it will be interesting to compare Gdax and kraken (or huobipro) since both of them have a compact fiat ratio change in first graph, but the performance are very different. This is due to spurious leader-follower relationship, which is shown in Section 5.3.

## Volume
In theory, we want to trade as many as possible when MA and BAS are the same.

```{r, echo = FALSE}
dt = ddply(dt_trans,~date,summarise,sum=sum(vol))
ggplot(data = dt)+
  geom_line(aes(x = date, y = sum)) +
  ylab('eth volume') 
```

The maximum daily eth volume is 500 (not include gdax as I don't have the account yet), which shows a high potential for our traders. It is worth to notice the trade volume changes a lot across time, all the exchanges exhibit the same pattern (surge around 19 March)

```{r, echo = FALSE}
dt_trans %>% 
  dplyr::group_by(exchange,date) %>% 
  dplyr::summarise(vol_date = sum(vol)) %>% 
  ggplot()+
  geom_line(aes(x=date,y=vol_date))+
  facet_wrap(~exchange,nrow=3)
  
```

The main driver for the total volume is bitcoincoid (notice that I have not transfered money to bitcoincoid on 20 March). For bitcoincoid and quadrigacx, the higher the volume the higher the profit, however for kraken, it is the opposite. We will see why in the next section.

## Predict Power
This section use the MA data to simulate traders' performance in an 'idea situation'. Again I do not have the gdax transcation data.

### Current CP with threshold = 0 
```{r, echo = FALSE}
predict = dt2 %>% 
  #filter(exchange == c('bitcoincoid','bitbay','kraken','gdax','independentreserve','quadrigacx','huobipro','livecoin')) %>% 
mutate(
  di_arb_nowday = arb_day - arb_now, # positive buy
  di_arb_nowhour = arb_hour - arb_now,
  mid = (market_bid + market_ask)/2,
  profit_index_mid = ifelse((di_arb_nowday>0 & Lag(mid,-1)-mid>=0)|(di_arb_nowday<0 & Lag(mid,-1)-mid<=0),1,0),
  profit_index_mid_bid = ifelse((di_arb_nowday>0 & Lag(mid,-1)-mid>0)|(di_arb_nowday<0 & Lag(mid,-1)-mid<=0),1,0),
  profit_index_mid_ask = ifelse((di_arb_nowday>0 & Lag(mid,-1)-mid>=0)|(di_arb_nowday<0 & Lag(mid,-1)-mid<0),1,0),
  profit_index_mid_bas = ifelse((di_arb_nowday>0 & Lag(mid,-1)-mid>0)|(di_arb_nowday<0 & Lag(mid,-1)-mid<0),1,0),
  profit_index = ifelse((di_arb_nowday>0 & Lag(market_price,-1)-market_price>=0)|(di_arb_nowday<0 & Lag(market_price,-1)-market_price<=0),1,0),
  price_mid_change = Lag(mid,-1)-mid
) 

predict_power = aggregate(predict$profit_index, by=list(exchange=predict$exchange), FUN=mean,na.rm=TRUE)

trade_thres = 0.0000
predict2 = predict %>% 
  filter(di_arb_nowday>(trade_thres)|di_arb_nowday<(-trade_thres)) %>% 
  mutate(
    profit_index_threshold = ifelse((di_arb_nowday>trade_thres & Lag(market_price,-1)-market_price>=0)|(di_arb_nowday<(-trade_thres) & Lag(market_price,-1)-market_price<=0),1,0)
  )
predict_power2 = aggregate(predict2$profit_index_threshold, by=list(exchange=predict2$exchange), FUN=mean,na.rm=TRUE)

ggplot(data = predict_power2) +
  geom_col(aes(x=reorder(exchange, -x),y=x)) +
  ylab ( 'correct rate using threshold =0') +
  xlab ('exchange') +
  geom_text(aes(x=reorder(exchange, -x),y=x,label = round(x,3)))

```

The correct porion (CP) measures the proportion of how many time MA correctly predict next 20 mins price direction. For example, if at 00:00, MA now - MA daily = 0.000000001, MA predicts the price will decrease, if 00:20 the price decreases, then MA corrects. In the next part, we set threshold = 0.0001, which means, only when |MA now - MA daily| > 0.0001 the trader trades, in other words, if MA now - MA daily = 0.000000001, the trader doesn't trade. This can avoid ambiguous MA signal, which is particularly crucial in 'dramatically change' market situations. For example, the market price is going to decrease very fast, some of the algo traders may realize the smell of crash but ours not, even though our MA is negative, but it is not negative enough, so the market keep filling our bid but not ask. We could implement by change the "maMidArb" as a piecewise function in cal.js.

The best performer is bitcoincoid, and the worst is kraken and huobipro. This explain why even though the volume of kraken and huobipro traders are high but the profits are limited. Even though the excution is effective, as the innital prediction is wrong, the result could not be right. Why the prediction is so poor? We will discuss it in Section 5.3 

The opposite of kraken is Independent reserve (IR), which has the second highest predict rate, but the performance is still low. I guess this is because the excution rate and the fee, it is hard to trade in IR (4.2.1) and the fee is higher (4.3), and the higher fee cause even less trades. Moreover, from the individual balance part, we notice the fiat ratio is very low for IR, which further proves the execution power of MA for IR is very low (too much fraction; exchange fee and low volume; smater competitor?).


### Current CP with threshold = 0.0001
```{r, echo = FALSE}
trade_thres = 0.0001
predict2 = predict %>% 
  filter(di_arb_nowday>(trade_thres)|di_arb_nowday<(-trade_thres)) %>% 
  mutate(
    profit_index_threshold = ifelse((di_arb_nowday>trade_thres & Lag(market_price,-1)-market_price>=0)|(di_arb_nowday<(-trade_thres) & Lag(market_price,-1)-market_price<=0),1,0)
  )
predict_power2 = aggregate(predict2$profit_index_threshold, by=list(exchange=predict2$exchange), FUN=mean,na.rm=TRUE)

ggplot(data = predict_power2) +
  geom_col(aes(x=reorder(exchange, -x),y=x)) +
  ylab ( 'correct rate using threshold =0.001') +
  xlab ('exchange') +
  geom_text(aes(x=reorder(exchange, -x),y=x,label = round(x,3)))

```

### Current CP with threshold = 0.0005
```{r, echo = FALSE}
trade_thres = 0.0005
predict2 = predict %>% 
  filter(di_arb_nowday>(trade_thres)|di_arb_nowday<(-trade_thres)) %>% 
  mutate(
    profit_index_threshold = ifelse((di_arb_nowday>trade_thres & Lag(market_price,-1)-market_price>=0)|(di_arb_nowday<(-trade_thres) & Lag(market_price,-1)-market_price<=0),1,0)
  )
predict_power2 = aggregate(predict2$profit_index_threshold, by=list(exchange=predict2$exchange), FUN=mean,na.rm=TRUE)

ggplot(data = predict_power2) +
  geom_col(aes(x=reorder(exchange, -x),y=x)) +
  ylab ( 'correct rate using threshold =0.005') +
  xlab ('exchange') +
  geom_text(aes(x=reorder(exchange, -x),y=x,label = round(x,3)))

```


Unfortunately it is hard to see significant increase of the CP when adjusting the threshold, I have created an interactive interface to fine-tune the threshold to find the highest CP for each exchange, we could further incorporate this parameters to cal.js.

Based on the CP, I would like to keep  bitcoincoid > gdax > bitbay > livecoin > quadrigacx  these five traders.

### Interactive threshold
```{r, echo = FALSE}

sliderInput("threshold", "threshold:", min = 0, max = 0.01, value = 0)
predict = dt2 %>% 
  #filter(exchange == c('bitcoincoid','bitbay','kraken','gdax','independentreserve','quadrigacx','huobipro','livecoin')) %>% 
mutate(
  di_arb_nowday = arb_day - arb_now, # positive buy
  di_arb_nowhour = arb_hour - arb_now,
  mid = (market_bid + market_ask)/2,
  profit_index_mid = ifelse((di_arb_nowday>0 & Lag(mid,-1)-mid>=0)|(di_arb_nowday<0 & Lag(mid,-1)-mid<=0),1,0),
  profit_index_mid_bid = ifelse((di_arb_nowday>0 & Lag(mid,-1)-mid>0)|(di_arb_nowday<0 & Lag(mid,-1)-mid<=0),1,0),
  profit_index_mid_ask = ifelse((di_arb_nowday>0 & Lag(mid,-1)-mid>=0)|(di_arb_nowday<0 & Lag(mid,-1)-mid<0),1,0),
  profit_index_mid_bas = ifelse((di_arb_nowday>0 & Lag(mid,-1)-mid>0)|(di_arb_nowday<0 & Lag(mid,-1)-mid<0),1,0),
  profit_index = ifelse((di_arb_nowday>0 & Lag(market_price,-1)-market_price>=0)|(di_arb_nowday<0 & Lag(market_price,-1)-market_price<=0),1,0),
  price_mid_change = Lag(mid,-1)-mid
) 

predict_power = aggregate(predict$profit_index, by=list(exchange=predict$exchange), FUN=mean,na.rm=TRUE)

trade_thres = 0.0000
predict2 = predict %>% 
  filter(di_arb_nowday>(trade_thres)|di_arb_nowday<(-trade_thres)) %>% 
  mutate(
    profit_index_threshold = ifelse((di_arb_nowday>trade_thres & Lag(market_price,-1)-market_price>=0)|(di_arb_nowday<(-trade_thres) & Lag(market_price,-1)-market_price<=0),1,0)
  )
predict_power2 = aggregate(predict2$profit_index_threshold, by=list(exchange=predict2$exchange), FUN=mean,na.rm=TRUE)



renderPlot({
  predict2 = predict %>% 
  filter(di_arb_nowday>(input$threshold)|di_arb_nowday<(-input$threshold)) %>% 
  mutate(
    profit_index_threshold = ifelse((di_arb_nowday>input$threshold & Lag(market_price,-1)-market_price>=0)|(di_arb_nowday<(-input$threshold) & Lag(market_price,-1)-market_price<=0),1,0)
  )
predict_power2 = aggregate(predict2$profit_index_threshold, by=list(exchange=predict2$exchange), FUN=mean,na.rm=TRUE)

ggplot(data = predict_power2) +
  geom_col(aes(x=reorder(exchange, -x),y=x)) +
  ylab ( 'correct rate using threshold =0') +
  xlab ('exchange') +
  geom_text(aes(x=reorder(exchange, -x),y=x,label = round(x,3)))
})
```

# Traders Characteristics

This section summarize the specific characteristics for traders.

## arb 

```{r, echo = FALSE, warning = FALSE}


##  arb now - day: positive -> sell
ggplot(data = dt2) +
  geom_boxplot(aes(x=time,y=dif_nd,group = 1)) +
  facet_wrap(~exchange,nrow=3)

## arb day - month: positive -> sell 
ggplot(data = dt2) +
  geom_boxplot(aes(x=time,y=dif_dm,group = 1)) +
  facet_wrap(~exchange,nrow=3)

ggplot(data = dt2) +
  geom_line(aes(x=time,y=dif_nd,group = 1)) +
  facet_wrap(~exchange,nrow=3)

```

The first graph shows the boxplot for difference between arb now - day, the second is the difference b/w arb day - month. Except huobipro, all of them look good (mean = 0). Vertically comparing the first two graphd we found the scale of now-day is much larger than the scale of day-month. Based on the fact that the the diff_dm successfully predict the price decreasing, we may want to put more weight on this parameter? By increasing the weight on the additional profit in cal.js.

It is very surprising how similar the diff_arb among different exchanges. I expect there are more heterogeneity. Also the higher the |diff|, the more certain MA prediction will be for the future price, which should lead more profit (or less loss in declining market).

Notice here huobipro diff_dm is negative (all other trades are positive), which means MA predicts the price will increase overtime, which is proved to be wrong. And huobipro diff_nd is very close to 0, which provides much more ambiguous signal to the traders. These two facts further validify my decision of giving up huobipro.



## Volume
### Total Volume
```{r, echo = FALSE}
ggplot(data = global_vol)  +
  geom_bar(mapping = aes(x = reorder(name, volume), y=volume),stat = "identity") +
  xlab("exchange") +
  ylab('daily average eth volume') +
  geom_text(aes(x=reorder(name, volume),y=volume,label = round(volume,3)))

```

### Volume in Days
```{r, echo = FALSE}
dt_trans %>% 
  dplyr::group_by(exchange,date) %>% 
  dplyr::summarise(vol_date = sum(vol)) %>% 
  ggplot()+
  geom_line(aes(x=date,y=vol_date))+
  facet_wrap(~exchange,nrow=3)
  
```

## Fee
```{r, echo = FALSE}
dt_trans %>% 
  dplyr::group_by(exchange,date) %>% 
  dplyr::summarise(fee_date = mean(feeper)) %>% 
  ggplot()+
  geom_line(aes(x=date,y=fee_date))+
  facet_wrap(~exchange,nrow=3)

```


Fee: IR > bitbay > quadrigacx > huobi(0.2) > kraken > livecoin > bitcoincoid

# Exchanges Characteristics

## Eth Volume
```{r,message=FALSE, error=FALSE, echo=F, warning=F}
ggplot(data = dt2) +
  geom_boxplot(aes(x=time,y=market_ethvol,group = 1)) +
  facet_wrap(~exchange,nrow=3)

```


It is possible kraken and huobipro are the market leaders (at least not the follower).

## Bid Ask Spread

```{r,message=FALSE, error=FALSE, echo=F, warning=FALSE}
ggplot(data = dt2) +
  geom_boxplot(aes(x=time,y=bas,group = 1)) +
  facet_wrap(~exchange,nrow=3)

```


BAS is calculated using ask/bid
Livecoin has the highest BAS, means it will be profitbale in the stable market condition

## Leader - follower Relationship

Using Granger causality test here we prove the previous guess that Kraken is perhaps the market leader.

Notice here we should use the market price data from gdax eth/usd, but unfortunately I didn't have time to fetch the data yet. By using this approach, we could easily test the leader-follower relationship between different exchanges, which could be very helpful to select future profitable exchanges. 

### kraken -> quadrigacx

```{r,message=FALSE, error=FALSE, echo=F, warning=FALSE}
p_kraken = dt2 %>% 
  filter(exchange == 'kraken') 

p_quadrigacx = dt2 %>% 
  filter(exchange == 'quadrigacx') 

model = cbind(p_quadrigacx$market_price,p_kraken$market_price)
colnames(model)=c('quadrigacx','kraken')

# Granger test 
var.2c <- VAR(model, p = 2, type = "const")
causality(var.2c, cause = "kraken")
causality(var.2c, cause = "quadrigacx")

```

### kraken -> huobi

```{r,message=FALSE, error=FALSE, echo=F, warning=FALSE}
## kraken  -> Huobipro ############################# 
p_kraken = dt2 %>% 
  filter(exchange == 'kraken') 

p_huobipro = dt2 %>% 
  filter(exchange == 'huobipro') 

model = cbind(p_huobipro$market_price,p_kraken$market_price)
colnames(model)=c('huobipro','kraken')

# Granger test 
var.2c <- VAR(model, p = 2, type = "const")
causality(var.2c, cause = "kraken")
causality(var.2c, cause = "huobipro")
```


# Conclusion

* Profitability: bitcoincoid > quadrigacx > livecoin > bitbay > IR > kraken > huobipro

* MA correct rate: 
    + prediction power: bitcoincoid > IR > bitbay > livecoin > quadrigacx > huobipro > kraken
    
    + arb_now - day: Give up Huobipro
    
    + arb_day - month: Give up Huobipro


kraken and huobipro are very likely to be the market leader (at least not the follower by granger test)


* MA fraction 

    + Fee: bitcoincoid > livecoin > kraken > huobi > quadrigacx > IR
    
    + Volume: bitcoincoid > quadrigacx > kraken > bitbay > livecoin > IR
    
    + lb (excution of MA by looking at the 0.25/0.75 quantile of bal ratio): bitcoincoid > quadrigacx > IR > livecoin > huobipro > bitbay > kraken
    
    + Exchange service (potential close down risk): I can't log in sometimes in Huobipro, livecoin stop API access for 5 days
    
* BAS: livecoin > bitbay > quddrigacx > IR > bitcoincoid > huobipro > kraken

Together, I would like to keep gdax, bitcoincoid, quadrigacx and Wex (maybe livecoin and bitbay). I reckon it is luck that livecoin drop only 10% as livecoin.com stop the API access and that 5 days price dropped a lot and we only have 0.04 eth in livecoin. Otherwise I think it will perform much worse in the downward market condition. Unless you believe price will be stable in the upcoming future (livecoin has the highest BAS). However, it performed the best of all my trader in the last week. I will keep livecoin for April to see how it goes in total. By the way, Wex is making profit even when the market is going downward (the slack balance didn't report the total balance as the API only provide available balance which exclude the on trade volume), I have no time to get the data to present here though. 

# Suggestion

Based on the above analysis, I want to 

* Transfer huobipro, IR and kraken's money into Wex (around 11eth)


* Increase the MA power for bitcoincoid, quadrigacx and livecoin (the volume is too small atm) by 
      
      + Increase the coeffcient of MA to from 1 to 1.01
      
      + Decrease the lb profit level from 0.005 to 0.0025
      
      + Decrease the gb profit level from 0.02 to 0.005
      
# Profitability Goal

* Current time, total Balance (TB) and current price: 
```{r, echo=FALSE}
total$time[nrow(total)]
total$bal_total[nrow(total)]
total$market_price[nrow(total)]
```

* Target Balance in April 30: 
    + if prices fall 10 - 30% (min:**280**): TB drop 0 - 10% (**97,078**)
    + if prices fall 5 - 10%: TB drop 0 - 5%
    + if prices fall 0 - 5% : TB rises 0 - 2% 
    + if prices rise 0 - 5%: TB rises 2 - 8%
    + if prices rise 5 - 20%: TB rises 5 - 25%
    + if prices rise 20 - 40% (max:**560**): TB rises 15 - 50% (**161,797**)
    
From the previous March market condition, I roughly know how my traders perform in a bad and stable market condition, so the range of bear market is narrower than the bull market.

I assume the price will change range from -30 to 40% in April. 


Predictions:

* if market is falling (10 - 30%): bitcoincoid should be the best performer 

* if market is stable (-10 to 10%): Livecoin 

* if market is rising (10 - 40%): quadrigacx 


# Task Goal
  + Maintain current 6 traders(bitcoincoid, bitbay, quadrigacx, gdax, livecoin, wex)
  + Analyise the April performance of the traders, add risk measurement (sharp ratio, VaR and standard deviation)
  + Adjust capitals in different traders based on April performance
  + Search for potential eth margin trade (short position) platform 
  + New market - alt coin (BTC/ETH), leader(binance) and followers (livecoin;kraken;wex,gdax,quadrigacx, bitcoincoid)
  
  -- 1) Write price fetcher (btc/eth) from the above exchanges; do lead-lag relationship test to choose 3 exchanges 
  
  -- 2) Establish a new server to host altcoin traders
  
  -- 3) Establish a new Firebase to host arb/bal/prices_binance
  
  -- 4) Create a new repo with updated CCXT, I want to use th latest ccxt to write the new code
 
  -- 5) If possible make 1 trader alive 
  



# Notes

* Eth events: https://cryptocalendar.pro/events/ethereum

* Bitcoin events: https://bravenewcoin.com/industry-resources/events-calendar/bitcoin-and-blockchain-events/

* Eth prices: https://ethereumprice.org/ 

